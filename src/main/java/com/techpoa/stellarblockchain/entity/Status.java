package com.techpoa.stellarblockchain.entity;

public enum Status {
    ACCOUNT_CREATED, WALLET_CREATED;
}
