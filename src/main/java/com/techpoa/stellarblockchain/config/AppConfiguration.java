package com.techpoa.stellarblockchain.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class AppConfiguration {

    @Value("${app.http.connection.timeout}")
    int httpConnectionTimeout;

    @Value("${app.http.read.timeout}")
    int httpReadTimeout;

    @Value("${stellar.network.url}")
    String stellarNetworkUrl;

    @Value("${stellar.network.friendbot}")
    String stellarNetworkFriendlyBot;

}
