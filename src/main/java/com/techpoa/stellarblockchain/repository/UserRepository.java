package com.techpoa.stellarblockchain.repository;


import com.techpoa.stellarblockchain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

}
