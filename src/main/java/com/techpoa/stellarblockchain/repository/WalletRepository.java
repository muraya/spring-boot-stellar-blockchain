package com.techpoa.stellarblockchain.repository;

import com.techpoa.stellarblockchain.entity.User;
import com.techpoa.stellarblockchain.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
    Wallet findByUser(User user);
}
