package com.techpoa.stellarblockchain.dto;

import lombok.Builder;

@Builder
public record UserDto(String email) {}
