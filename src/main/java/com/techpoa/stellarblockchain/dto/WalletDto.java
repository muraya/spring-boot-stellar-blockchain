package com.techpoa.stellarblockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class WalletDto {

    private String publicKey;

    private String privateKey;

    private String accountName;

}
