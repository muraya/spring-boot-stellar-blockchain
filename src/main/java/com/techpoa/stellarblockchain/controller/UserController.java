package com.techpoa.stellarblockchain.controller;

import com.techpoa.stellarblockchain.dto.UserDto;
import com.techpoa.stellarblockchain.entity.User;
import com.techpoa.stellarblockchain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UserController {

    private final UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {

        return ResponseEntity.ok().body(userService.getUsers());
    }
    @PostMapping("/users")
    public ResponseEntity<User> createUser(@RequestBody UserDto userDto)
    {
        return ResponseEntity.ok().body(userService.saveUser(userDto));
    }

    @GetMapping("/users/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable("email") String email) {
        Optional<User> tutorialData = userService.findUserByEmail(email);

        return tutorialData.map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
