package com.techpoa.stellarblockchain.service;


import com.techpoa.stellarblockchain.dto.WalletDto;
import com.techpoa.stellarblockchain.entity.Wallet;

import java.util.List;
import java.util.Optional;

public interface WalletService {



    public List<Wallet> getWallets();

    public Wallet saveWallet(WalletDto walletDto);

    public Optional<Wallet> findUserWallet(long id);

}