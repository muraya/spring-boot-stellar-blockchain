package com.techpoa.stellarblockchain.service;


import com.techpoa.stellarblockchain.config.AppConfiguration;
import com.techpoa.stellarblockchain.dto.WalletDto;
import com.techpoa.stellarblockchain.entity.User;
import com.techpoa.stellarblockchain.entity.Wallet;
import com.techpoa.stellarblockchain.repository.WalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
@Slf4j
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    AppConfiguration configuration;

    public List<Wallet> getWallets(){
        log.info("Fetching all wallets from database ");
        return walletRepository.findAll();
    }

    public Wallet saveWallet(WalletDto walletDto){

        log.info("Saving new wallet to the database with name {} ", walletDto.getAccountName());

        Wallet wallet = Wallet.builder()
                .publicKey(walletDto.getPublicKey())
                .privateKey(walletDto.getPrivateKey())
                .accountName(walletDto.getAccountName())
                .build();

        return walletRepository.save(wallet);

    }

    public Optional<Wallet> findUserWallet(long id){
        return walletRepository.findById(id);
    }


}
