package com.techpoa.stellarblockchain.service;

import com.techpoa.stellarblockchain.dto.UserDto;
import com.techpoa.stellarblockchain.entity.Status;
import com.techpoa.stellarblockchain.entity.User;
import com.techpoa.stellarblockchain.entity.Wallet;
import com.techpoa.stellarblockchain.repository.UserRepository;
import com.techpoa.stellarblockchain.repository.WalletRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final WalletRepository walletRepository;

    @Override
    public List<User> getUsers() {
        log.info("Fetching all users from database ");
        return userRepository.findAll();
    }

    @Override
    public User saveUser(UserDto userDto) {

        log.info("Saving new user to the database {} ", userDto.email());
        User user = User.builder()
                .email(userDto.email())
                .status(Status.ACCOUNT_CREATED)
                .created_at(Date.from(Instant.now()))
                .build();

        User userResponse = userRepository.save(user);

      Wallet wallet =  Wallet.builder()
                .publicKey("qewqeweq")
                .privateKey("qwexweqw")
                .accountName(user.getEmail())
                .balance(0)
                .user(userResponse)
                .build();

        walletRepository.save(wallet);

        return userResponse;


    }

    @Override
    public Optional<User> findUserByEmail(String email) {

        log.info("Fetching user by email {} ", email);
        return Optional.of(userRepository.findByEmail(email));
    }
}
