package com.techpoa.stellarblockchain.service;


import com.techpoa.stellarblockchain.dto.UserDto;
import com.techpoa.stellarblockchain.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public List<User> getUsers();

    public User saveUser(UserDto user);

    public Optional<User> findUserByEmail(String email);

}
