package com.techpoa.stellarblockchain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStellarBlockchainApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootStellarBlockchainApplication.class, args);
    }

}
